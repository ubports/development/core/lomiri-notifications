qt5_wrap_ui(GUI_HEADERS mainWindow.ui serverMainWindow.ui clientMainWindow.ui)

# Viewer app
############

add_executable(viewer
  viewer.cpp
  mainWindow.cpp
  mainWindow.h
  ${GUI_HEADERS}
)

target_link_libraries(viewer
  Qt5::Widgets
  notifybackend
)

# QMLtest app
#############

add_executable(qmltest
  qmltest.cpp
)

target_link_libraries(qmltest
  Qt5::Qml
  Qt5::Quick
  Qt5::DBus
  Qt5::Widgets
  notifybackend
)

# DBus server app
#################

add_executable(dbusserver
  dbusserver.cpp
  serverMainWindow.cpp
  ${GUI_HEADERS}
)

target_link_libraries(dbusserver
  Qt5::Widgets
  Qt5::DBus
  notifybackend
)

# DBus client app
#################

add_executable(dbusclient
  dbusclient.cpp
  clientMainWindow.cpp
  ${GUI_HEADERS}
)

target_link_libraries(dbusclient
  Qt5::Widgets
  Qt5::DBus
  notifybackend
)

# Move files around
###################

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/Notifications
    DESTINATION ${CMAKE_BINARY_DIR}/tools
)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/datatest.qml
    DESTINATION ${CMAKE_BINARY_DIR}/tools
)
